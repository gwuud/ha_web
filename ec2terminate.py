import boto3
import sys
import os


def main(event, context):

    tag_value = os.environ['INSTANCE_TAG']

    ec2 = boto3.client('ec2')
    try:
        instances = ec2.describe_instances(
            Filters = [
                {
                    'Name': 'tag:Name',
                    'Values': [
                        tag_value
                    ]
                }
            ]
        )
    except RuntimeError as err:
        print("{}: Failed to create filtered object".format(err))
        sys.exit(1)

    instance_ids = get_instance_ids(instances)
    ec2.terminate_instances(InstanceIds = instance_ids)


def get_instance_ids(instances):

    app_instances = []
    for reservation in instances['Reservations']:
        for instance in reservation['Instances']:
            instance_id = instance['InstanceId']
            app_instances.append(instance_id)
    return app_instances
