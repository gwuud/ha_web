#!/bin/bash

CfnCreateStack() {
    aws cloudformation create-stack             \
    --stack-name $NAME                          \
    --template-body file://$TEMPLATE            \
    --parameters file://$PARAMS                 \
    --region=$REGION                            \
    --capabilities CAPABILITY_IAM
}

CfnUpdateStack() {
    aws cloudformation update-stack             \
    --stack-name $NAME                          \
    --template-body file://$TEMPLATE            \
    --parameters file://$PARAMS                 \
    --region=$REGION                            \
    --capabilities CAPABILITY_IAM
}

CfnDeleteStack() {
    aws cloudformation delete-stack             \
    --stack-name $NAME
}

EC2CreateKey() {
    aws ec2 create-key-pair                     \
    --key-name $KEYNAME                         \
    --query 'KeyMaterial'                       \
    --output text >$KEYNAME.pem
    
    chmod 400 $KEYNAME.pem
}

EC2DeleteKey() {
    aws ec2 delete-key-pair --key-name $KEYNAME
}
